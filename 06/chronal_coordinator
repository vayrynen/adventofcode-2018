#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;
use IO::File;
use List::Util qw(min max);

my $basename = basename($0);

if(@ARGV != 1) {
    print "Usage: $basename <filename>\n";
    exit 1;
}

sub main {
    my $input_file = shift;
    my $input_fd = IO::File->new("$input_file", O_RDONLY) or
        die("Failed to open file \"$input_file\"\n");

    my @xs, my @ys, my @coords;
    while(<$input_fd>) {
        (my $x, my $y) = $_ =~ /(\d+), (\d+)/;
        # x, y, area, valid
        push @coords, [$x, $y, 0, 1];
        push @xs, $x;
        push @ys, $y;
    }

    my $min_x = min @xs;
    my $min_y = min @ys;
    my $max_x = max @xs;
    my $max_y = max @ys;

    for my $x($min_x - 1 .. $max_x + 1) {
        for my $y($min_y - 1 .. $max_y + 1) {
            my $best = -1;
            my $shortest_distance = 999999;
            my $valid = 1;
            for my $i(0 .. $#coords) {
                my $distance = abs($x - $coords[$i][0]) + abs($y - $coords[$i][1]);
                if ($distance < $shortest_distance) {
                    $shortest_distance = $distance;
                    $best = $i;
                    $valid = 1;
                }
                elsif ($distance == $shortest_distance) {
                    # Tied
                    $valid = 0;
                }
            }

            # Outside the grid - area is infinite, mark as invalid
            if ($x == $min_x-1 or $x == $max_x+1 or $y == $min_y-1 or $y == $max_y+1) {
                $coords[$best][3] = 0;
            }
            elsif ($valid) {
                $coords[$best][2]++;
            }
        }
    }

    my $largest_area = -1;
    for my $i(0 .. $#coords) {
        # For all finite areas
        if ($coords[$i][3]) {
            if ($coords[$i][2] > $largest_area) {
                $largest_area = $coords[$i][2];
            }
        }
    }

    print $largest_area . "\n";
}

main($ARGV[0]);
