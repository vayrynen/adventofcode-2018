#!/usr/bin/perl

use strict;
use warnings;
use feature qw(say);

use Data::Dumper;
use File::Basename;

if(@ARGV != 1) {
    say "Usage: " . basename $0 . " <filename>";
    exit 1;
}

sub main {
    my $input_file = shift;
    open(my $input_fd, '<', $input_file) or die("Failed to open input file \"$input_file\"");

    while(<$input_fd>) {
        # Do something
    }
}

main($ARGV[0]);
